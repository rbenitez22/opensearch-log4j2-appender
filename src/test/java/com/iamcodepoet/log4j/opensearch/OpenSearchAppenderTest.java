/*
 * Copyright 2022 Roberto C. Benitez.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.iamcodepoet.log4j.opensearch;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

/**
 *
 * @author Roberto C. Benitez
 */
public class OpenSearchAppenderTest
{

    private static final Logger LOG = LogManager.getLogger(OpenSearchAppenderTest.class);

    public OpenSearchAppenderTest()
    {
    }

    @Test
    public void testSomeMethod()
    {
        LOG.info("Info Message");
        LOG.warn("Warning Message");

        logIOException();
        Exception third = new FileNotFoundException("FileNotFoundException in a nested set of exceptions");
        Exception second = new IOException(third.getMessage(), third);
        Exception first = new RuntimeException(second.getMessage(), second);
        LOG.error(first.getMessage(), first);

        Thread thread = new Thread(() -> LOG.info("Log Entry from Thread"), "Test Thread");
        thread.setDaemon(true);
        thread.start();
    }

    public void logIOException()
    {
        try
        {
            byte[] bytes = Files.readAllBytes(Paths.get("non-existant-file.txt"));
            LOG.error("Error.  Not sure where '{}' came from--the file does not exists", bytes);
        }
        catch (IOException e)
        {
            LOG.error(e.getMessage(), e);
        }
    }

}
