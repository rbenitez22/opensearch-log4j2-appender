/*
 * Copyright 2022 Roberto C. Benitez.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.iamcodepoet.log4j.opensearch;

import java.time.LocalDate;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author Roberto C. Benitez
 */
public class JsonObjectTest
{
    
    public JsonObjectTest()
    {
    }
    
    @Test
    public void testSomeMethod()
    {
        JsonObject adaName = new JsonObject();
        adaName.put("firstName", "Ada");
        adaName.put("lastName", "Byron");
        
        JsonObject ada = new JsonObject();
        ada.put("name", adaName);
        ada.put("dob", LocalDate.of(1812, 12, 15));
        
        JsonObject lbName = new JsonObject();
        lbName.put("firstName", "Lord");
        lbName.put("LastName", "Byron");
        
        JsonObject lordByron = new JsonObject();
        lordByron.put("name", lbName);
        lordByron.put("dob", null);
        
        ada.put("spouse", lordByron);
        
        String expected = "{\"name\":{\"firstName\":\"Ada\",\"lastName\":\"Byron\"},\"dob\":\"1812-12-15\",\"spouse\":{\"name\":{\"firstName\":\"Lord\",\"LastName\":\"Byron\"},\"dob\":null}}";
        String actual = ada.createJsonString();
        Assert.assertEquals(expected, actual);
    }
    
}
