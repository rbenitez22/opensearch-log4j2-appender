/*
 * Copyright 2022 Roberto C. Benitez.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.iamcodepoet.log4j.opensearch;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Month;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author Roberto C. Benitez
 */
public class JsonMessageBuilderTest
{

    public JsonMessageBuilderTest()
    {
    }

    @Test
    public void testSomeMethod()
    {
        String expected = "{\"firstName\":\"Ada\",\"lastName\":\"Byron\",\"age\":39,\"dob\":\"1815-12-10\",\"wakeUptime \":\"05:30\",\"married\":true,\"children\":3,\"floatValue\":1.0,\"doubleValue\":2.0}";
        String actual = JsonMessageBuilder.builder()
                .addField("firstName", "Ada")
                .addField("lastName", "Byron")
                .addField("age", 39)
                .addField("dob", LocalDate.of(1815, Month.DECEMBER, 10))
                .addField("wakeUptime ", LocalTime.of(5, 30))
                .addField("married", true)
                .addField("children", 3)
                .addField("floatValue", 1f)
                .addField("doubleValue", 2d)
                .build();

        Assert.assertEquals(expected, actual);
    }

}
