/*
 * Copyright 2022 Roberto C. Benitez.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.iamcodepoet.log4j.opensearch;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.Appender;
import org.apache.logging.log4j.core.Core;
import org.apache.logging.log4j.core.Filter;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.appender.AbstractAppender;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.config.plugins.PluginAttribute;
import org.apache.logging.log4j.core.config.plugins.PluginElement;
import org.apache.logging.log4j.core.config.plugins.PluginFactory;
import org.apache.logging.log4j.status.StatusLogger;

/**
 *
 * @author Roberto C. Benitez
 */
@Plugin(
        name = "OpenSearchAppender",
        category = Core.CATEGORY_NAME,
        elementType = Appender.ELEMENT_TYPE)
public class OpenSearchAppender extends AbstractAppender
{

    private static final Logger LOGGER = StatusLogger.getLogger();

    private final MessageSender sender;
    private final AppenderField[] additionalFields;

    public OpenSearchAppender(String name, Filter filter, AppenderField[] additionalFields, MessageSender sender)
    {
        super(name, filter, null);

        this.additionalFields = additionalFields;
        this.sender = sender;

    }

    @Override
    public void append(LogEvent event)
    {
        try
        {
            sender.send(event, additionalFields);
        }
        catch (RuntimeException e)
        {
            Throwable cause = e.getCause() == null ? e : e.getCause();
            String error = String.format("Error sending message to Opensearch. %s", cause.getMessage());
            LOGGER.error(error, cause);
        }
    }

    @PluginFactory
    public static OpenSearchAppender createAppender(
            @PluginAttribute("name") String name,
            @PluginElement("Filter") Filter filter,
            @PluginElement("OsAppenderField") AppenderField[] fields,
            @PluginAttribute("host") String host,
            @PluginAttribute("port") int port,
            @PluginAttribute("ssl_trustall") boolean trustAll,
            @PluginAttribute("indexName") String indexName,
            @PluginAttribute("userName") String userName,
            @PluginAttribute("password") String password)
    {

        IndexClient indexClient = new IndexClient(host, port, indexName, userName, password);
        MessageSender sender = new MessageSender(indexClient);

        if (trustAll)
        {
            setupTrustManager();
        }

        if (!indexClient.isIndexpresent())
        {
            LOGGER.info("Index '{}' does not exist.  Attempting to create.", indexName);
            indexClient.createIndex();
            LOGGER.info("Created Index '{}'", indexName);
        }

        return new OpenSearchAppender(name, filter, fields, sender);

    }

    protected static void setupTrustManager()
    {
        try
        {
            SSLContext context = SSLContext.getInstance("SSL");
            context.init(null, createTrustAllManager(), new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(context.getSocketFactory());

        }
        catch (KeyManagementException | NoSuchAlgorithmException e)
        {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    private static TrustManager[] createTrustAllManager()
    {
        return new TrustManager[]
        {
            new X509TrustManager()
            {
                @Override
                public void checkClientTrusted(X509Certificate[] arg0, String arg1) throws CertificateException
                {
                    //trust all
                }

                @Override
                public void checkServerTrusted(X509Certificate[] arg0, String arg1) throws CertificateException
                {
                    //trust all
                }

                @Override
                public X509Certificate[] getAcceptedIssuers()
                {
                    return null;
                }
            }
        };
    }

}
