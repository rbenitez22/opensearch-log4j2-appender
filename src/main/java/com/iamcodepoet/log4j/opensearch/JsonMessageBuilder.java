/*
 * Copyright 2022 Roberto C. Benitez.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.iamcodepoet.log4j.opensearch;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.time.Instant;
import org.apache.logging.log4j.core.LogEvent;

/**
 * Build a JSON message string
 *
 * @author Roberto C. Benitez
 */
class JsonMessageBuilder
{

    private final JsonObject root = new JsonObject();

    private JsonMessageBuilder()
    {
    }

    protected JsonMessageBuilder addField(String name, Object value)
    {
        root.put(name, value);
        return this;
    }

    protected JsonMessageBuilder addFields(AppenderField... fields)
    {
        if (fields != null)
        {
            for (AppenderField field : fields)
            {
                addField(field.getName(), field.getValue());
            }
        }

        return this;
    }

    public String build()
    {
        return root.createJsonString();
    }

    protected static JsonMessageBuilder builder()
    {
        return new JsonMessageBuilder();
    }

    public static String createJsonMessage(LogEvent event, AppenderField[] additionalFields)
    {
        Instant timestamp = Instant.ofEpochSecond(event.getInstant().getEpochSecond());

        return builder()
                .addField("timestamp", timestamp.toString())
                .addField("stack_trace", convertStacktraceToString(event))
                .addField("message", event.getMessage().getFormattedMessage())
                .addField("level", event.getLevel().name())
                .addField("source", getSourceString(event))
                .addField("class_name", getClassName(event.getSource()))
                .addField("file_name", getFileName(event.getSource()))
                .addField("method_name", getMethodName(event.getSource()))
                .addField("line_number", getLineNumber(event.getSource()))
                .addField("thread_name", event.getThreadName())
                .addFields(additionalFields)
                .build();
    }

    private static String convertStacktraceToString(LogEvent event)
    {
        String thrown;
        if (event.getThrown() == null)
        {
            thrown = "";
        }
        else
        {
            StringWriter out = new StringWriter();
            PrintWriter pw = new PrintWriter(out, true);
            event.getThrown().printStackTrace(pw);
            thrown = out.toString();
        }
        return thrown;
    }

    private static String getSourceString(LogEvent event)
    {
        return event.getSource() == null ? "" : event.getSource().toString();
    }

    private static String getFileName(StackTraceElement stackTrace)
    {
        if (stackTrace == null)
        {
            return "";
        }

        return stackTrace.getFileName();
    }

    private static String getMethodName(StackTraceElement stackTrace)
    {
        if (stackTrace == null)
        {
            return "";
        }

        return stackTrace.getMethodName();
    }

    private static int getLineNumber(StackTraceElement stackTrace)
    {
        if (stackTrace == null)
        {
            return 0;
        }

        return stackTrace.getLineNumber();
    }

    private static String getClassName(StackTraceElement stackTrace)
    {
        if (stackTrace == null)
        {
            return "";
        }

        return stackTrace.getClassName();
    }

}
