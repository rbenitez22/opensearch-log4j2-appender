/*
 * Copyright 2022 Roberto C. Benitez.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.iamcodepoet.log4j.opensearch;

import java.util.Objects;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.Configuration;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.config.plugins.PluginAttribute;
import org.apache.logging.log4j.core.config.plugins.PluginConfiguration;
import org.apache.logging.log4j.core.config.plugins.PluginFactory;
import org.apache.logging.log4j.status.StatusLogger;
import org.apache.logging.log4j.util.Strings;

/**
 *
 * @author Roberto C. Benitez
 */
@Plugin(name = "OpenSearchField", category = "Core", printObject = true)
public class AppenderField
{

    private static final Logger LOGGER = StatusLogger.getLogger();

    private final String name;
    private final String value;

    public AppenderField(String name, String value)
    {
        this.name = name;
        this.value = value;
    }

    public String getName()
    {
        return name;
    }

    public String getValue()
    {
        return value;
    }

    @Override
    public int hashCode()
    {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(this.name);
        hash = 47 * hash + Objects.hashCode(this.value);
        return hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        final AppenderField other = (AppenderField) obj;
        if (!Objects.equals(this.name, other.name))
        {
            return false;
        }
        return Objects.equals(this.value, other.value);
    }

    @PluginFactory
    public static AppenderField createField(@PluginConfiguration final Configuration config,
            @PluginAttribute("name") String name,
            @PluginAttribute("value") String value)
    {

        if (Strings.isEmpty(name))
        {
            LOGGER.error("The name is empty");
            return null;
        }

        if (Strings.isEmpty(value))
        {
            LOGGER.error("The pattern, literal, and mdc attributes are mutually exclusive.");
            return null;
        }

        return new AppenderField(name, value);
    }

}
