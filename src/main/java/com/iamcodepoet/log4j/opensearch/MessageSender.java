/*
 * Copyright 2022 Roberto C. Benitez.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.iamcodepoet.log4j.opensearch;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.status.StatusLogger;

/**
 *
 * @author Roberto C. Benitez
 */
class MessageSender
{

    private static final Logger LOGGER = StatusLogger.getLogger();

    private final IndexClient indexClient;

    MessageSender(IndexClient indexClient)
    {
        this.indexClient = indexClient;
    }

    protected void send(LogEvent event, AppenderField[] additionalFields)
    {
        try
        {
            HttpURLConnection conn = indexClient.createMessagePostConnection();

            conn.connect();
            try ( OutputStream os = conn.getOutputStream();
                  BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os)))
            {
                String jsonString = JsonMessageBuilder.createJsonMessage(event, additionalFields);
                writer.write(jsonString);
            }
            finally
            {
                conn.disconnect();
            }

            int code = conn.getResponseCode();
            String msg = conn.getResponseMessage();
            if (!(code == 201 || code == 200))
            {
                String error = String.format("Unable to send log message. Status: '%s', Response '%s'", code, msg);
                LOGGER.error(error);
            }

        }
        catch (IOException e)
        {
            LOGGER.error(e.getMessage(), e);
        }
    }
}
