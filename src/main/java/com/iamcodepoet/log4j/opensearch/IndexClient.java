/*
 * Copyright 2022 Roberto C. Benitez.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.iamcodepoet.log4j.opensearch;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Base64;
import java.util.UUID;

/**
 *
 * @author Roberto C. Benitez
 */
class IndexClient
{

    private static final JsonObject INDEX_SETTINGS = new JsonObject()
            .put("number_of_shards", 2)
            .put("number_of_replicas", 1);

    private static final JsonObject MAPPINGS = MappingsBuilder.builder()
            .add("timestamp", "date")
            .add("stack_trace", "text")
            .add("message", "text")
            .add("level", "keyword")
            .add("source", "text")
            .add("class_name", "keyword")
            .add("file_name", "keyword")
            .add("method_name", "keyword")
            .add("line_number", "integer")
            .add("thread_name", "keyword")
            .build();

    private final String host;
    private final int port;
    private final String indexName;

    private final String userName;
    private final String password;

    IndexClient(String host, int port, String indexName, String userName, String password)
    {

        this.host = host.startsWith("http") ? host : "https://" + host;
        this.port = port;
        this.indexName = indexName;
        this.userName = userName;
        this.password = password;
    }

    private String createBasicAuth()
    {
        String userPass = userName + ":" + password;
        return "Basic " + Base64.getEncoder().encodeToString(userPass.getBytes());
    }

    private URL createMessagePostUrl()
    {

        return createUrl("/_doc/%s", UUID.randomUUID());
    }

    private URL createUrl(String template, Object... args)
    {
        try
        {
            String uri = String.format(template, args);
            String spec = String.format("%s:%s/%s%s", host, port, indexName, uri);
            return new URL(spec);
        }
        catch (MalformedURLException e)
        {
            throw new IllegalStateException(e.getMessage(), e);
        }
    }

    protected HttpURLConnection createMessagePostConnection() throws IOException
    {
        return createConnection("POST", createMessagePostUrl());
    }

    protected HttpURLConnection createConnection(String requestMethod, URL url)
    {
        try
        {
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod(requestMethod);
            conn.setRequestProperty("Content-Type", "application/json");
            String basicAuth = createBasicAuth();
            conn.setRequestProperty("Authorization", basicAuth);
            conn.setDoOutput(true);
            conn.setDoInput(true);

            conn.connect();

            return conn;
        }
        catch (IOException e)
        {
            throw new IllegalStateException(e.getMessage(), e);
        }
    }

    protected void createIndex()
    {
        HttpURLConnection conn = createConnection("PUT", createUrl(""));

        try ( OutputStream os = conn.getOutputStream())
        {
            JsonObject root = createIndexJson();

            os.write(root.createJsonString().getBytes());
        }
        catch (IOException e)
        {
            throw new IllegalStateException(e.getMessage(), e);
        }
        finally
        {
            conn.disconnect();
        }

    }

    protected boolean isIndexpresent()
    {
        try
        {
            return checkIfPresent();
        }
        catch (IOException e)
        {
            if (e instanceof FileNotFoundException)
            {
                return false;
            }
            throw new IllegalStateException(e.getMessage(), e);
        }
    }

    private boolean checkIfPresent() throws IOException
    {
        URL url = createUrl("");

        HttpURLConnection conn = createConnection("GET", url);

        StringWriter writer = new StringWriter();
        try ( InputStream is = conn.getInputStream())
        {
            while (true)
            {
                byte[] buff = new byte[1024];

                int count = is.read(buff);
                if (count < 1)
                {
                    break;
                }

                String string = new String(buff, 0, count);
                writer.write(string);
            }
        }
        finally
        {
            conn.disconnect();
        }

        int code = conn.getResponseCode();
        return (code == 201 || code == 200) && !writer.toString().isEmpty();
    }

    private static JsonObject createIndexJson()
    {
        JsonObject settings = new JsonObject();
        settings.put("index", INDEX_SETTINGS);
        JsonObject root = new JsonObject();
        root.put("settings", settings);
        root.put("mappings", MAPPINGS);
        return root;

    }

    private static class MappingsBuilder
    {

        private final JsonObject props = new JsonObject();

        private MappingsBuilder add(String name, String type)
        {
            JsonObject typeNode = new JsonObject();
            typeNode.put("type", type);

            JsonObject prop = new JsonObject();
            prop.put(name, typeNode);

            return this;
        }

        private JsonObject build()
        {
            JsonObject propsNode = new JsonObject();
            propsNode.put("properties", props);

            JsonObject mappings = new JsonObject();
            mappings.put("mappings", propsNode);

            return mappings;
        }

        private static final MappingsBuilder builder()
        {
            return new MappingsBuilder();
        }
    }

}
