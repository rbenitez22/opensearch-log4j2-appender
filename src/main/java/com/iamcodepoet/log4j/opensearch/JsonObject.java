/*
 * Copyright 2022 Roberto C. Benitez.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.iamcodepoet.log4j.opensearch;

/**
 *
 * @author Roberto C. Benitez
 */
class JsonObject
{

    private final StringBuilder message = new StringBuilder();

    protected JsonObject put(String name, Object value)
    {
        if (message.length() > 0)
        {
            message.append(',');
        }

        message.append(quote(name));
        message.append(':');

        String stringValue = createJsonValue(value);
        message.append(stringValue);
        
        return this;
    }
    
    protected String createJsonString()
    {
        return "{" + message + "}";
    }

    @Override
    public String toString()
    {
        return createJsonString();
    }
    
    private static String createJsonValue(Object object){
        if(object == null){
            return "null";
        }
        
        final String jsonValue;
        if(object instanceof String)
        {
            jsonValue = escapeAndQuoteString((String)object);
        }
        else if(object instanceof Double)
        {
            Double dbl = (Double)object;
            if(dbl.isInfinite() || dbl.isNaN())
            {
                jsonValue = "null";
            }
            else
            {
                jsonValue = dbl.toString();
            }
        }else if(object instanceof Float)
        {
            Float flt = (Float)object;
            if(flt.isInfinite() || flt.isNaN())
            {
                jsonValue = "null";
            }
            else
            {
                jsonValue = flt.toString();
            }
        }
        else if(object instanceof Number)
        {
            jsonValue = ((Number)object).toString();
        }
        else if(object instanceof Boolean)
        {
            jsonValue = ((Boolean)object).toString();
        }
        else if(object instanceof JsonObject)
        {
            jsonValue = ((JsonObject)object).createJsonString();
        }
        else
        {
            jsonValue = escapeAndQuoteString(object.toString());
        }
        
        return jsonValue;
    }

    private static String escapeAndQuoteString(String string)
    {
        return  quote(escape(string));
    }
    
    private static String quote(String string)
    {
        return "\"" + string + "\"";
    }
    
    private static String escape(String string) 
    {
        StringBuilder sb = new StringBuilder();
        for(char ch : string.toCharArray())
        {
            switch(ch)
            {
                case '"':
                    sb.append("\\\"");
                    break;
                case '\\':
                    sb.append("\\\\");
                    break;
                case '\b':
                    sb.append("\\b");
                    break;
                case '\f':
                    sb.append("\\f");
                    break;
                case '\n':
                    sb.append("\\n");
                    break;
                case '\r':
                    sb.append("\\r");
                    break;
                case '\t':
                    sb.append("\\t");
                    break;
                case '/':
                    sb.append("\\/");
                    break;
                default:
                    escapeUnicode(ch, sb);  
            }
        }
        
        return sb.toString();
    }

    private static void escapeUnicode(char ch, StringBuilder sb)
    {
        //Reference: http://www.unicode.org/versions/Unicode5.1.0/
        if((ch>='\u0000' && ch<='\u001F') || (ch>='\u007F' && ch<='\u009F') || (ch>='\u2000' && ch<='\u20FF'))
        {
            String ss=Integer.toHexString(ch);
            sb.append("\\u");
            for(int k=0;k<4-ss.length();k++)
            {
                sb.append('0');
            }
            sb.append(ss.toUpperCase());
        }
        else
        {
            sb.append(ch);
        }
    }
}
